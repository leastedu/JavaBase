//
// Least Edu ----Learn It Easier!
// @project:   https://gitee.com/leastedu
// @web:       https://www.idreamschool.com
// @email:     leastedu@sina.com

public class ForceTrans {
    public static void main(String[] args) {
        int i1 = 123;
        byte b = (byte) i1;
        //强制类型转换为byte
        System.out.println("int强制类型转换为byte后的值等于" + b);
    }
}