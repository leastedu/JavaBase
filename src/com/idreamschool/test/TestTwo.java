//
// Least Edu ----Learn It Easier!
// @project:   https://gitee.com/leastedu
// @web:       https://www.idreamschool.com
// @email:     leastedu@sina.com


//package com.idreamschool.test;
//这里到package打开时，javac可以通过，但是java TestTwo会报错：
//错误: 找不到或无法加载主类 TestTwo
//原因: java.lang.ClassNotFoundException: TestTwo

public class TestTwo {
    public void pupAge() {
        int age = 0;
        age = age + 7;
        System.out.println("小狗的年龄是 : " + age);
    }

    public static void main(String[] args) {
        TestTwo test = new TestTwo();
        test.pupAge();
    }
}